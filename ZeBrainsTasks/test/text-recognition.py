import json

import cv2 as cv
import base64
import requests

URL = 'http://127.0.0.1:8000/text-recognition/'


def decode(image_name: str):
    ext = '.' + image_name.split('/')[-1].split('.')[-1]
    image = cv.imread(image_name)
    _, buffer = cv.imencode(ext, image)
    query = json.dumps({'image':  str(base64.b64encode(buffer).decode())})
    return query


response = requests.post(URL, data=decode('test1.jpg'))
print(response.json())
# {'response': ['MyShared']}

response = requests.post(URL, data=decode('test2.jpg'))
print(response.json())
# {'response': ['10-14-2021', 'Thu', '16:58:16', 'P', 'Camera', '01']}

response = requests.post(URL, data=decode('test3.jpg'))
print(response.json())
# {'response': ['English Sample', 'M Ainor', 'pructiced']}