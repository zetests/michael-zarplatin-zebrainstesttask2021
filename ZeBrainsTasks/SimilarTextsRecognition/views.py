from django.http import JsonResponse
from rest_framework.decorators import api_view

from .bert import calc_cos_similarity

import json


@api_view(['POST'])
def similar_recognition(request):

    if request.method == 'POST':

        texts = json.loads(request.body)['texts']

        result = calc_cos_similarity(texts)

        return JsonResponse({'response': str(result[0, 0])})
