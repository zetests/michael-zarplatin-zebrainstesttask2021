from django.urls import path

from .views import *


urlpatterns = [
    path('similar-recognition/', similar_recognition)
]