from sentence_transformers import SentenceTransformer
from sklearn.metrics.pairwise import cosine_similarity


model = SentenceTransformer('bert-base-nli-mean-tokens')


def calc_cos_similarity(texts):
    sentence_embeddings = model.encode(texts)

    return cosine_similarity([sentence_embeddings[0]], [sentence_embeddings[1]])
