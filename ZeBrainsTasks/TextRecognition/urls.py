from django.urls import path

from .views import *

urlpatterns = [
    path('text-recognition/', text_recognition)
]