from django.http import JsonResponse
from rest_framework.decorators import api_view
from .ocr import recognize

import cv2 as cv
import numpy as np

import base64
import json


@api_view(['POST'])
def text_recognition(request):

    if request.method == 'POST':
        jpg_original = base64.b64decode(json.loads(request.body)['image'])

        jpg_as_np = np.frombuffer(jpg_original, dtype=np.uint8)
        image = cv.imdecode(jpg_as_np, flags=1)

        texts = recognize(image)

        return JsonResponse({'response': texts})
