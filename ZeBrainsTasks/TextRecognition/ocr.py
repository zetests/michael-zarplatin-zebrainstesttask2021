from easyocr import Reader


reader = Reader(['en'])


def recognize(image) -> list:
    results = reader.readtext(image)

    texts = []
    for (_, text, prob) in results:
        print(f'[INFO]: {prob}: {text}')

        if prob > 0.5:
            texts.append(text)

    return texts
