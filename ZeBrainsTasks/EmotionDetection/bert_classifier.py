from transformers import pipeline


classifier = pipeline("text-classification", model='bhadresh-savani/distilbert-base-uncased-emotion',
                      return_all_scores=True)


def detect(text):
    result = classifier(text)

    return result
