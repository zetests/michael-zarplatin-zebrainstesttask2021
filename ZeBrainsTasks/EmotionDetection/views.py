from django.http import JsonResponse
from rest_framework.decorators import api_view

from .bert_classifier import detect

import json


@api_view(['POST'])
def emotion_detection(request):

    if request.method == 'POST':

        text = json.loads(request.body)['text']

        result = detect(text)

        return JsonResponse({'response': result[0]})
