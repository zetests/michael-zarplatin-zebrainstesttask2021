# Тестовое задание в ZeBrains(2021)

Серверная часть реализована при помощи Django Rest Framwork.  
python version == 3.9  
**Установка необходимых инструментов в командной строке(cmd):**
```
pip install django
pip install djangorestframework
pip install sklearn
pip install numpy
pip install opencv-python
pip install easyocr
pip install sentence-transformers
```
**Для запуска сервера на localhost нужно перейти в папку (cmd) с файлом _manage.py_(_ZeBrainsTasks_) и выполнить команду:**  
```
python manage.py runserver
```
После этого можно работать с сервисами.

Тестовое задание состоит из 3 задач:

## 1. Emotion Detection
### В чем суть:
Мы получаем какой-то текст, а модель на серверной части должна вернуть список эмоций и вероятностей этих эмоций, которыми по мнению модельки окрашен текст.
![Image1](https://miro.medium.com/max/1838/1*QbnbVSRoAyqLG76ZtF-oew.png)
### Как воспользоваться:
По endpoint'у "http://127.0.0.1:8000/emotion-detection/" отправить в теле POST запроса JSON вида:
```
    {
        "text": "Your Text"
    }
```
, после чего будет получен json вида:
```
    {
        "response": [
            {
                "label": "Some Label",
                "score": floatScore
            },
            ...
    }
```
***text*** - текст, эмоцию которого мы должны осознать;  
***label*** - метка, обозначающая эмоцию (например, joy - радость);  
***score*** - число, обозначающее вероятность эмоции  

Была использована [модель](https://huggingface.co/bhadresh-savani/distilbert-base-uncased-emotion?text=I+hate+it.) на основе языковой модели BERT, а точнее [DistillBERT](https://arxiv.org/pdf/1910.01108.pdf)  
## 2. Text Recogition
### В чем суть:
Мы получаем на вход картинку, а модель на серверной части должна вернуть текст, который изображен на картинке.
![Image2](https://929687.smushcdn.com/2407837/wp-content/uploads/2020/09/easyocr_swedish_output.jpg?lossy=1&strip=1&webp=1)
### Как воспользоваться:
По endpoint'у "http://127.0.0.1:8000/text-recognition/" отправить в теле POST запроса JSON вида:
```
    {
        "image": "Encoded image"
    }
```
, после чего будет получен JSON с распознанным текстом:
```
    {
        "response": [
                "Text1",
                "Text2",
                ...
        ]
    }
```

***image*** - картинка, которая должна быть предварительно закондирована в определенный формат  
Код для кодирования изображения:
```
import cv2 as cv
import base64

image = cv.imread('path/to/image.jpg')

_, buffer = cv.imencode('.jpg', image)

jpg_as_text = base64.b64encode(buffer)

jpg_as_text = str(jpg_as_text.decode())
```
Установка библиотеки opencv(cmd):
```
pip install opencv-python
```

Для считывания текста была использована библиотека [EasyOCR](https://github.com/JaidedAI/EasyOCR)

## 3. Similar Text Recognition:
### В чем суть:
На вход приложению поступает два текста, а с него приходит ответ с похожестью текстов от 0.0 до 1.0 (0% - 100%).
![Image3](https://miro.medium.com/max/1313/0*6tiGwCmZclFesyDe.png)
### Как воспользоваться:
По endpoint'у "http://127.0.0.1:8000/similar-recognition/" отправить POST запрос. содержащий в теле JSON типа: 
```
{
    "texts": [
        "Text1",
        "Text2"
    ]
}
```
На выходе получаем JSON вида:
```
{
    "response": "Float Value"
}
```

Был использован алгоритм на основе языковой модели [BERT](https://arxiv.org/pdf/1810.04805.pdf) для формирования эмбеддингов, на основе которых вычисляется косинусовое расстояние.
